import java.sql.Date;
import java.util.Iterator;

public class MyDate {
	
	int day, month, year;
	MyDate(int day, int month, int year){
		this.day = day;
		this.month = month;
		this.year = year;
	}
	public String toString(){
		boolean dday = (this.day >= 0 && this.day < 10);
		boolean mmonth = (this.month >= 1 && this.month < 10);
		
		String day = String.valueOf(this.day);
		String month = String.valueOf(this.month);
		String year = String.valueOf(this.year);
		
		if (dday && mmonth){
			return year + "-0" + month + "-0" + day;  
		}
		else if (dday){
			return year + "-" + month + "-0" + day;  
		}
		else if (mmonth){
			return year + "-0" + month + "-" + day;  
		}
		else{
			return year + "-" + month + "-" + day;  
		}
	}
	//day+
	public void incrementDay(){
		if ((this.year%4 == 0) && this.month == 2){
			
			if(this.day==29){
				this.month+=1;
				this.day=1;
				}
			else{
				this.day+=1;
			}
		}
		else if ((this.year%4!=0) && this.month == 2){			
			if(this.day==28){
				this.month+=1;
				this.day=1;
			}				
			else{
				this.day+=1;
			}
		}
		else if ((this.month==1) || (this.month==3) || (this.month==5) || (this.month==7) || (this.month==8) || (this.month==10)){			
			if(this.day==31){
				this.month+=1;
				this.day=1;
			}				
			else{
				this.day+=1;
			}
		}

		else if ((this.month==4) || (this.month==6) || (this.month==9) || (this.month==11)){	
			if(this.day==30){
				this.month+=1;
				this.day=1;
			}
			else{
				this.day+=1;
			}
		}
		else if (this.month==12){	
			if(this.day==31){
				this.month=1;
				this.day=1;
				this.year+=1;
			}
				
			else{
				this.day+=1;
			}
		}
	}

	public void incrementDay(int value) {
		while (value>0) {
			incrementDay();
			value-=1;
		}
	}
	//month+
	public void incrementMonth(int value) {
		this.month+=value;
		while (this.month > 12) {
			this.month-=12;
			this.year+=1;
		}
		if (this.month==2) {
			if(this.year%4 == 0 && this.day>29) {
				this.day=29;
			}
			else if(this.year%4 != 0 && this.day>28){
				this.day=28;
			}
		}
		else if ((this.month==4) || (this.month==6) || (this.month==9) || (this.month==11)){
			if(this.day >30) {
				this.day=30;
			}
		}
	}
	public void incrementMonth() {
		incrementMonth(1);
	}
	//year+
	public void incrementYear() {
		if(this.month==2 && this.day==29) {
			this.year+=1;
			this.day=28;
		}
		else {
			this.year+=1;
		}
	}
	public void incrementYear(int value) {
		if(value%4==0) {
			this.year+=4;
		}
		else {
			while (value>0) {
				incrementYear();
				value-=1;
			}
		}	
	}
	//day-
	public void decrementDay() {
		if ((this.year%4 == 0) && this.month == 3){	
			if(this.day==1){
				this.month-=1;
				this.day=29;
			}
				
			else{
				this.day-=1;
			}
		}
		else if ((this.year%4!=0) && this.month == 3){	
			if(this.day==1){
				this.month-=1;
				this.day=28;
			}
			else{
				this.day-=1;
			}
		}
		else if ((this.month==2) || (this.month==4) || (this.month==6) || (this.month==8) || (this.month==9) || (this.month==11)){
			if(this.day==1){
				this.month-=1;
				this.day=31;
			}	
			else{
				this.day-=1;
			}
		}
		else if ((this.month==5) || (this.month==7) || (this.month==10) || (this.month==12)){	
			if(this.day==1){
				this.month-=1;
				this.day=30;
			}	
			else{
				this.day-=1;
			}
		}
		else if (this.month==1){	
			if(this.day==1){
				this.month=12;
				this.day=31;
				this.year-=1;
			}	
			else{
				this.day-=1;
			}
		}			
	}
	public void decrementDay(int value){
		while(value>0){
			decrementDay();
			value-=1;		
		}
	}
	//month-
	public void decrementMonth(int value){
		while (value>0) {
			if(this.month==1) {
				this.month=12;
				this.year-=1;
			}
			else {
				this.month-=1;
			}
			value-=1;
		}
		if (this.month==2) {
			if(this.year%4 == 0 && this.day>29) {
				this.day=29;
			}
			else if(this.year%4 != 0 && this.day>28){
				this.day=28;
			}
		}
		else if ((this.month==4) || (this.month==6) || (this.month==9) || (this.month==11)){
			if(this.day >30) {
				this.day=30;
			}
		}	
	}
	public void decrementMonth() {
		if(this.month==1) {
			this.month=12;
		}
		else {
			this.month-=1;
		}
		if (this.month==2) {
			if(this.year%4 == 0 && this.day>29) {
				this.day=29;
			}
			else if(this.year%4 != 0 && this.day>28){
				this.day=28;
			}
		}
		else if ((this.month==4) || (this.month==6) || (this.month==9) || (this.month==11)){
			if(this.day >30) {
				this.day=30;
			}
		}	
	}
	//year-
	public void decrementYear() {
		if(this.month==2 && this.day==29) {
			this.year-=1;
			this.day=28;
		}
		else {
			this.year-=1;
		}
	}
	public void decrementYear(int value){
		if(value%4==0) {
			this.year-=4;
		}
		else {
			while (value>0) {
				decrementYear();
				value-=1;
			}
		}
	}
	public boolean isBefore(MyDate newDate){
		if (this.year > newDate.year){
			return false;
		}
		else if (this.year == newDate.year){
			if(this.month > newDate.month){
				return false;
			}
			else if (this.month == newDate.month){
				if (this.day>=newDate.day) {
					return false;
				}
				else{
					return true;
				}
			}
			else{
				return true;
			}
		}
		else{
			return true;
		}
	}
	public boolean isAfter(MyDate newDate){
		if (this.year < newDate.year){
			return false;
		}
		else if (this.year == newDate.year){
			if(this.month < newDate.month){
				return false;
			}
			else if (this.month == newDate.month){
				if (this.day<=newDate.day) {
					return false;
				}
				else{
					return true;
				}
			}
			else{
				return true;
			}
		}
		else{
			return true;
		}
	}
	public int dayDifference(MyDate newDate){
		int differenceDay=0;
		MyDate calculationOfDays = new MyDate(this.day, this.month, this.year);
		boolean equality = true;
		// (calculationOfDays.day==newDate.day && calculationOfDays.month==newDate.month && calculationOfDays.year==newDate.year)==true;
		if (this.isAfter(newDate)){
			while(equality){
				calculationOfDays.decrementDay();
				differenceDay+=1;
				if(calculationOfDays.day==newDate.day && calculationOfDays.month==newDate.month && calculationOfDays.year==newDate.year){
					equality=false;
					return differenceDay;
				}
			}
		}
		else if (this.isBefore(newDate)){
			while(equality){
				calculationOfDays.incrementDay();
				differenceDay+=1;
				if(calculationOfDays.day==newDate.day && calculationOfDays.month==newDate.month && calculationOfDays.year==newDate.year){
					equality=false;
					return differenceDay;
				}
			}
		}
		else{
			return differenceDay;
		}
		return differenceDay;
		
	}
}



